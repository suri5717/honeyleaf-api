/**
 * TenantStay.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
    tableName: 'tenant_stay_history',
    autoUpdatedAt: false,
    attributes: {
        tenant: {
            model: 'tenant',
            columnName: 'tenant_id'
        },
        bed: {
            model: 'bed',
            columnName: 'bed_id'
        },
        fromDate: {
            type: 'date',
            columnName: 'from_date'
        },
        toDate: {
            type: 'date',
            columnName: 'to_date'
        },
        createdAt: {
            type: 'datetime',
            columnName: 'created_at'
        },
        toJSON: function() {
            var obj = this.toObject();
            var moment = require('moment');
            if(this.fromDate && moment(this.fromDate).isValid()) obj.fromDate = moment(this.fromDate).format('YYYY-MM-DD');
            if(this.toDate && moment(this.toDate).isValid()) obj.toDate = moment(this.toDate).format('YYYY-MM-DD');
            if(this.createdAt && moment(this.createdAt).isValid()) obj.createdAt = moment(this.createdAt).format('YYYY-MM-DD HH:mm:ss');
            return obj;
        }
    }
};
