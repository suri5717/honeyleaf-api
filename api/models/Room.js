/**
 * Room.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
    tableName: 'rooms',
    attributes: {
        name: {
            type: 'string'
        },
        floorNo: {
            type: 'integer',
            columnName: 'floor_no'
        },
        pg: {
            model: 'pg',
            columnName: 'pg_id'
        },
        beds: {
            collection: 'bed',
            via: 'roomId'
        },
        curBedCount: {
            type: 'integer',
            columnName: 'cur_bed_count'
        },
        maxBedCount: {
            type: 'integer',
            columnName: 'max_bed_count'
        },
        amenities: {
            type: 'string'
        },
        createdAt: {
            type: 'datetime',
            columnName: 'created_at'
        },
        updatedAt: {
            type: 'datetime',
            columnName: 'updated_at'
        },
        toJSON: function() {
            var obj = this.toObject();
            var moment = require('moment');
            if(this.createdAt && moment(this.createdAt).isValid()) obj.createdAt = moment(this.createdAt).format('YYYY-MM-DD HH:mm:ss');
            if(this.updatedAt && moment(this.updatedAt).isValid()) obj.updatedAt = moment(this.updatedAt).format('YYYY-MM-DD HH:mm:ss');
            return obj;
        }
    }
};