/**
 * PgPayment.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
    tableName: 'pg_payments',
    attributes: {
        pg: {
            model: 'pg',
            columnName: 'pg_id'
        },
        to: {
            type: 'string'
        },
        category: {
            type: 'string',
            defaultsTo: 'others'
        },
        amount: {
            type: 'float'
        },
        notes: {
            type: 'text'
        },
        billDate: {
            type: 'date',
            columnName: 'date'
        },
        billUrl: {
            type: 'string',
            columnName: 'bill_url'
        },
        createdAt: {
            type: 'datetime',
            columnName: 'created_at'
        },
        updatedAt: {
            type: 'datetime',
            columnName: 'updated_at'
        },
        toJSON: function() {
            var obj = this.toObject();
            var moment = require('moment');
            if (this.billDate && moment(this.billDate).isValid()) obj.billDate = moment(this.billDate).format('YYYY-MM-DD');
            if (this.createdAt && moment(this.createdAt).isValid()) obj.createdAt = moment(this.createdAt).format('YYYY-MM-DD HH:mm:ss');
            if (this.updatedAt && moment(this.updatedAt).isValid()) obj.updatedAt = moment(this.updatedAt).format('YYYY-MM-DD HH:mm:ss');
            return obj;
        }
    }
};
