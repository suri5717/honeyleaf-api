/**
 * TenantPayment.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
    tableName: 'tenant_payments',
    attributes: {
        pgId: {
            model: 'pg',
            columnName: 'pg_id'
        },
        tenant: {
            model: 'tenant',
            columnName: 'tenant_id'
        },
        amount: {
            type: 'integer'
        },
        monthPaidForDate: {
            type: 'date',
            columnName: 'month_paid_for'
        },
        paidDate: {
            type: 'date',
            columnName: 'paid_date'
        },
        type: {
            type: 'string',
            enum: ['monthly_rent', 'one_time', 'deposit'],
            defaultsTo: 'monthly_rent'
        },
        createdAt: {
            type: 'datetime',
            columnName: 'created_at'
        },
        updatedAt: {
            type: 'datetime',
            columnName: 'updated_at'
        },
        toJSON: function() {
            var obj = this.toObject();
            var moment = require('moment');
            if(this.monthPaidForDate && moment(this.monthPaidForDate).isValid()) obj.monthPaidForDate = moment(this.monthPaidForDate).format('YYYY-MM-DD');
            if(this.paidDate && moment(this.paidDate).isValid()) obj.paidDate = moment(this.paidDate).format('YYYY-MM-DD');
            if(this.createdAt && moment(this.createdAt).isValid()) obj.createdAt = moment(this.createdAt).format('YYYY-MM-DD HH:mm:ss');
            if(this.updatedAt && moment(this.updatedAt).isValid()) obj.updatedAt = moment(this.updatedAt).format('YYYY-MM-DD HH:mm:ss');
            delete obj.pg;
            return obj;
        }
    }
};
