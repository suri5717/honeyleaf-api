/**
 * Bed.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/#!documentation/models
 */

module.exports = {
    tableName: 'beds',
    attributes: {
        pgId: {
            model: 'pg',
            columnName: 'pg_id'
        },
        roomId: {
            model: 'room',
            columnName: 'room_id'
        },
        tenantId: {
            model: 'tenant',
            columnName: 'tenant_id'
        },
        bookedTenantId: {
            model: 'tenant',
            columnName: 'booked_tenant_id'
        },
        isVacant: {
            type: 'boolean',
            columnName: 'is_vacant'
        },
        isBooked: {
            type: 'boolean',
            columnName: 'is_booked'
        },
        createdAt: {
            type: 'datetime',
            columnName: 'created_at'
        },
        updatedAt: {
            type: 'datetime',
            columnName: 'updated_at'
        },
        toJSON: function() {
            var obj = this.toObject();
            if(!!obj.roomId && typeof(obj.roomId) == 'object'){
                obj.room = obj.roomId;
                delete obj.roomId;
            }
            if(!!obj.tenantId && typeof(obj.tenantId) == 'object'){
                obj.tenant = obj.tenantId;
                delete obj.tenantId;
            }
            if(!!obj.bookedTenantId && typeof(obj.bookedTenantId) == 'object'){
                obj.bookedTenant = obj.bookedTenantId;
                delete obj.bookedTenantId;
            }

            delete obj.pgId;

            var moment = require('moment');
            if(this.createdAt && moment(this.createdAt).isValid()) obj.createdAt = moment(this.createdAt).format('YYYY-MM-DD HH:mm:ss');
            if(this.updatedAt && moment(this.updatedAt).isValid()) obj.updatedAt = moment(this.updatedAt).format('YYYY-MM-DD HH:mm:ss');
            return obj;
        }
    }
};
