/**
* Tenant.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
    tableName: 'tenants',
    attributes: {
        name: {
            type: 'string'
        },
        email: {
            type: 'string'
        },
        mobile: {
            type: 'string'
        },
        altMobile: {
            type: 'string',
            columnName: 'alt_mobile',
            defaultsTo: null
        },
        address: {
            type: 'string'
        },
        type: {
            type: 'string',
            enum: ['regular', 'guest'],
            defaultsTo: 'regular'
        },
        bed: {
            model: 'bed',
            columnName: 'bed_id'
        },
        pg: {
            model: 'pg',
            columnName: 'pg_id'
        },
        payments: {
            collection: 'tenantPayment',
            via: 'tenant'
        },
        photoUrl: {
            type: 'string',
            columnName: 'photo_url'
        },
        fromDate: {
            type: 'date',
            columnName: 'from_date'
        },
        toDate: {
            type: 'date',
            columnName: 'to_date'
        },
        status: {
            type: 'string',
            enum: ['occupied','notice','booked', 'left'],
            defaultsTo:  'occupied'
        },
        noticeStartDate: {
            type: 'date',
            columnName: 'notice_start_date'
        },
        noticeEndDate: {
            type: 'date',
            columnName: 'notice_end_date'
        },
        createdAt: {
            type: 'datetime',
            columnName: 'created_at'
        },
        updatedAt: {
            type: 'datetime',
            columnName: 'updated_at'
        },
        toJSON: function() {
            var obj = this.toObject();
            var moment = require('moment');
            if(this.fromDate && moment(this.fromDate).isValid()) obj.fromDate = moment(this.fromDate).format('YYYY-MM-DD');
            if(this.toDate && moment(this.toDate).isValid()) obj.toDate = moment(this.toDate).format('YYYY-MM-DD');
            if(this.noticeStartDate && moment(this.noticeStartDate).isValid()) obj.noticeStartDate = moment(this.noticeStartDate).format('YYYY-MM-DD');
            if(this.noticeEndDate && moment(this.noticeEndDate).isValid()) obj.noticeEndDate = moment(this.noticeEndDate).format('YYYY-MM-DD');
            if(this.createdAt && moment(this.createdAt).isValid()) obj.createdAt = moment(this.createdAt).format('YYYY-MM-DD HH:mm:ss');
            if(this.updatedAt && moment(this.updatedAt).isValid()) obj.updatedAt = moment(this.updatedAt).format('YYYY-MM-DD HH:mm:ss');
            return obj;
        }
    }
};

