/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */

var passport = require('passport');
module.exports = function(req, res, next) {
    passport.authenticate('jwt', function(error, user, info) {
        if (error || !user) {
            return res.send({
                error: true,
                statusCode: 403,
                statusMessage: 'Session Expired! Please login again!!',
                isLoggedIn: false
            });
        }

        req.user = user;
        next();
    })(req, res);
};