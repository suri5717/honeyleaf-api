/**
 * TenantController
 *
 * @description :: Server-side logic for managing tenants
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var _ = require('underscore');
var async = require('async');
var moment = require('moment');

module.exports = {
    index: function(req, res) {
        var user = req.user;
        if (!user)
            return res.send({
                error: true,
                statusCode: 404,
                statusMessage: 'Session Expired. Please login again!'
            });

        if (!user.isOwner())
            return res.send({
                error: true,
                statusCode: 403,
                statusMessage: 'Not authorized to view this data'
            });

        var queries = {
            tenants: function(cb) {
                Tenant.find({ pg: user.pg })
                    .populate('bed')
                    .exec(function(err, tenants) {
                        if (err)
                            return res.send({
                                error: true,
                                statusCode: 500,
                                statusMessage: "Something went wrong",
                                err: err
                            });
                        cb(err, tenants);
                    });
            },
            rooms: ['tenants', function(cb, results) {
                var tenants = _.filter(results.tenants, function(tenant) {
                    return tenant.bed && tenant.bed.roomId;
                });
                var roomIds = _.map(tenants, function(tenant) {
                    return tenant.bed.roomId;
                });
                Room.find({ id: roomIds })
                    .exec(function(err, rooms) {
                        if (err)
                            return res.send({
                                error: true,
                                statusCode: 500,
                                statusMessage: "Something went wrong while finding rooms",
                                err: err
                            });

                        cb(err, rooms);
                    });
            }],
            map: ['rooms', function(cb, results) {
                var rooms = _.indexBy(results.rooms, 'id');
                var tenants = results.tenants
                _.each(tenants, function(tenant) {
                    if (tenant.bed && tenant.bed.roomId) {
                        tenant.room = rooms[tenant.bed.roomId].toJSON();
                        delete tenant.room.beds;
                    }
                });
                cb(null, tenants);
            }]
        };

        async.auto(queries, function(err, results) {
            if (err)
                return res.send({
                    error: true,
                    statusCode: 500,
                    statusMessage: 'Something went wrong'
                });

            res.send({
                success: true,
                statusCode: 200,
                statusMessage: 'done successfully',
                data: { tenants: results.map }
            });
        });
    },

    get: function(req, res) {
        var user = req.user;
        if (!user)
            return res.send({
                error: true,
                statusCode: 404,
                statusMessage: 'Session Expired. Please login again!'
            });

        if (!user.isOwner())
            return res.send({
                error: true,
                statusCode: 403,
                statusMessage: 'Not authorized to view this data'
            });

        var tenantId = req.param('tenantId');
        var queries = {
            tenant: function(cb) {
                Tenant.findOne({ id: tenantId, pg: user.pg })
                    .populateAll()
                    .exec(function(err, tenant) {
                        if (err)
                            return res.send({
                                error: true,
                                statusCode: 500,
                                statusMessage: "Something went wrong",
                                err: err
                            });
                        if (!tenant)
                            return res.send({
                                error: true,
                                statusCode: 404,
                                statusMessage: 'Tenant is not found with the specified tenant id ' + tenantId
                            });
                        cb(err, tenant);
                    });
            },
            room: ['tenant', function(cb, results) {
                var tenant = results.tenant;
                if (tenant.bed && tenant.bed.roomId) {
                    Room.findOne({ id: tenant.bed.roomId })
                        .exec(function(err, room) {
                            if (err)
                                return res.send({
                                    error: true,
                                    statusCode: 500,
                                    statusMessage: "Something went wrong while finding room with id " + tenant.bed.roomId,
                                    err: err
                                });
                            if (room) {
                                tenant.room = room.toJSON();
                                delete tenant.room.beds;
                            }
                            cb(null, tenant);
                        });
                } else {
                    cb(null, tenant);
                }
            }]
        };

        async.auto(queries, function(err, results) {
            if (err)
                return res.send({
                    error: true,
                    statusCode: 500,
                    statusMessage: 'Something went wrong'
                });

            res.send({
                success: true,
                statusCode: 200,
                statusMessage: 'done successfully',
                data: { tenant: results.room }
            });
        });
    },

    create: function(req, res) {
        var user = req.user;
        if (!user)
            return res.send({
                error: true,
                statusCode: 404,
                statusMessage: 'Session Expired. Please login again!'
            });

        if (!user.isOwner())
            return res.send({
                error: true,
                statusCode: 403,
                statusMessage: 'Not authorized to create a tenant'
            });

        var data = req.body;
        var errors = validateCreateTenantData(data);
        if (errors.length) {
            return res.send({
                error: true,
                statusCode: 400,
                statusMessage: errors.join(', ')
            });
        }

        /*
            -- basic validation errors --
            1. Check whether bed is free to assign this tenant
                1. bed must be is_vacant and not is_booked  OR
                2. tenant assigned to this bed must be in notice
                    1. notice end date must be less than the data.fromDate
            2. if valid
                1. create entry in tenants table
                2. update bed entry with tenant id
                3. new entry in tenant_stay_history
        */
        var queries = {
            isValid: function(cb) {
                Bed.findOne(data.bedId).populateAll()
                    .exec(function(err, bed) {
                        bed = bed.toJSON();
                        var error;
                        if (bed.bookedTenant) {
                            error = 'Bed is booked already';
                        } else if (bed.tenant) {
                            var tenant = bed.tenant;
                            if (tenant.status == 'occupied' || (tenant.status == 'notice' && moment(tenant.noticeEndDate).isAfter(data.fromDate))) {
                                error = 'Bed is occupied by some other tenant';
                            }
                        }
                        if (!!error)
                            return res.send({
                                error: true,
                                statusCode: 400,
                                statusMessage: error
                            });

                        cb(null, true);
                    });
            },
            createTenant: ['isValid', function(cb, results) {
                var tenantData = getTenantCreateData(data, user);
                Tenant.create(tenantData).exec(function(err, tenant) {
                        if (err || !tenant)
                            return res.send({
                                error: true,
                                statusCode: 500,
                                statusMessage: 'Something went wrong while creating tenant',
                                err: err
                            });

                        cb(null, tenant);
                    });
            }],
            updateBed: ['createTenant', function(cb, results) {
                var tenant = results.createTenant;
                var data;
                if (moment(tenant.fromDate).isAfter(moment())) {
                    data = {
                        bookedTenantId: tenant.id,
                        isBooked: true
                    }
                } else {
                    data = {
                        tenantId: tenant.id,
                        isVacant: false
                    }
                }
                Bed.update({ id: tenant.bed }, data).exec(function(err, bed) {
                    if (err) {
                        Tenant.destroy(tenant.id)
                            .exec(function(err, tenant) {
                                return res.send({
                                    error: true,
                                    statusCode: 500,
                                    statusMessage: 'Something went wrong while updating bed'
                                });
                            });
                    } else {
                        cb(null, bed);
                    }
                })
            }],
            tenantHistory: ['createTenant', 'updateBed', function(cb, results) {
                var tenant = results.createTenant;
                var data = {
                    tenant: tenant.id,
                    bed: tenant.bed,
                    fromDate: moment(tenant.fromDate).format('YYYY-MM-DD')
                }
                TenantStay.create(data).exec(function(err, tenantStay) {
                    if (err)
                        console.log(err);
                    tenant.bed = results.updateBed[0];
                    cb(null, tenant);
                });
            }]
        };

        async.auto(queries, function(err, results) {
            if (err)
                return res.send({
                    error: true,
                    statusCode: 500,
                    statusMessage: 'Something went wrong'
                });

            res.send({
                success: true,
                statusCode: 200,
                statusMessage: 'Tenant created successfully',
                data: { tenant: results.tenantHistory }
            });
        });
    },

    addPayment: function(req, res) {
        var user = req.user;
        if (!user)
            return res.send({
                error: true,
                statusCode: 404,
                statusMessage: 'Session Expired. Please login again!'
            });

        if (!user.isOwner())
            return res.send({
                error: true,
                statusCode: 403,
                statusMessage: 'Not authorized to view this data'
            });

        var data = req.body;
        if (!data.amount)
            return res.send({
                error: true,
                statusCode: 400,
                statusMessage: 'Amount is missing'
            });

        var paymentData = {
            tenant: req.param('tenantId'),
            amount: data.amount,
            type: data.paymentType || 'monthly_rent',
            monthPaidForDate: data.monthPaidForDate || moment().format('YYYY-MM-01'),
            paidDate: moment().format('YYYY-MM-DD')
        };

        Tenant.findOne({ id: paymentData.tenant, pg: user.pg })
            .populate('payments')
            .exec(function(err, tenant) {
                if (err)
                    return res.send({
                        error: true,
                        statusCode: 500,
                        statusMessage: "Something went wrong",
                        err: err
                    });

                if (!tenant)
                    return res.send({
                        error: true,
                        statusCode: 404,
                        statusMessage: 'Tenant is not found with the specified tenant id ' + tenantId
                    });

                // Tenant found, check whether payment is already done
                var prevPaymentMade;
                if (paymentData.type == 'monthly_rent') {
                    var payableDate = moment(paymentData.monthPaidForDate);
                    prevPaymentMade = _.find(tenant.payments, function(payment) {
                        var paymentDate = moment(payment.monthPaidForDate);
                        return payment.type == 'monthly_rent' &&
                            paymentDate.year() == payableDate.year() &&
                            paymentDate.month() == payableDate.month();
                    });
                } else if (paymentData.type == 'deposit') {
                    prevPaymentMade = _.find(tenant.payments, function(payment) {
                        return payment.type == 'deposit';
                    });
                }

                if (prevPaymentMade)
                    return res.send({
                        error: true,
                        statusCode: 400,
                        statusMessage: 'Payment was already paid by tenant to the specified(or current) month'
                    });

                TenantPayment.create(paymentData)
                    .exec(function(err, payment) {
                        if (err || !payment)
                            return res.send({
                                error: true,
                                statusCode: 500,
                                statusMessage: 'Something went wrong'
                            });

                        res.send({
                            success: true,
                            statusCode: 200,
                            statusMessage: 'Payment is added successfully',
                            data: { tenant: _.extend(tenant, { newPayment: payment.toJSON() }) }
                        });
                    });
            });
    }
};

var validateCreateTenantData = function(data) {
    var errors = [];
    if (!data.name) errors.push('Name is mandatory field');
    if (!data.email) errors.push('Email is mandatory field');
    if (!data.mobile) errors.push('Mobile is mandatory field');
    if (!data.type) errors.push('Type(Regular, Guest) is not specified');
    if (!data.bedId) errors.push('Bed is not chosen');
    if (!data.fromDate) errors.push('From Date is mandatory field');
    return errors;
}

var getTenantCreateData = function(data, user) {
    var tenantData = _.pick(data, 'name', 'email', 'mobile', 'altMobile', 'address', 'type');
    tenantData.fromDate = moment(data.fromDate).format('YYYY-MM-DD');
    tenantData.pg = user.pg;
    tenantData.bed = data.bedId;
    tenantData.status = moment(tenantData.fromDate).isAfter(moment()) ? 'booked' : 'occupied';
    return tenantData;
}
