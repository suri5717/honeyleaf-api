/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var _ = require('underscore');
var async = require('async');
var moment = require('moment');

module.exports = {
    profile: function(req, res) {
        var user = req.user;
        if (!user)
            return res.send({
                error: true,
                statusCode: 404,
                statusMessage: 'Session Expired. Please login again!'
            });

        User.findOne(user.id)
            .populate('pg')
            .exec(function(err, user) {
                if (err)
                    return res.send({
                        error: true,
                        statusCode: 500,
                        statusMessage: "Something went wrong",
                        err: err
                    });
                res.send({
                    success: true,
                    statusCode: 200,
                    statusMessage: 'done successfully',
                    data: {
                        user: user
                    }
                });
            });
    },
    launch: function(req, res) {
        var user = req.user;
        if (!user)
            return res.send({
                error: true,
                statusCode: 404,
                statusMessage: 'Session Expired. Please login again!'
            });

        var startDate = moment().subtract(1, 'months').startOf('month').format('YYYY-MM-DD');
        var endDate = moment().subtract(1, 'months').endOf('month').format('YYYY-MM-DD');

        var queries = {
            bedStatus: function(cb) {
                Bed.find({ pgId: user.pg })
                    .populate('tenantId')
                    .exec(function(err, beds) {
                        if (err || !beds)
                            return res.send({
                                error: true,
                                statusCode: 500,
                                statusMessage: "Something went wrong",
                                err: err
                            });

                        var data = { total: 0, occupied: 0, notice: 0, upcoming: 0, vacant: 0 };
                        data.total = beds.length;
                        _.each(beds, function(bed) {
                            bed = bed.toJSON();
                            if (bed.isVacant) {
                                bed.isBooked ? data.upcoming++ : data.vacant++;
                            } else {
                                bed.tenant && bed.tenant.status == 'notice' ? data.notice++ : data.occupied++;
                            }
                        });

                        cb(err, data);
                    });
            },
            income: function(cb) {
                TenantPayment.find({ pgId: user.pg, paidDate: { '>=': startDate, '<=': endDate } })
                    .exec(function(err, payments) {
                        if (err) return cb(false, 0);

                        totalIncome = _.reduce(payments, function(sum, payment) {
                            return parseFloat(payment.amount) + sum;
                        }, 0);
                        cb(false, totalIncome * 100);
                    });
            },
            expenses: function(cb) {
                PgPayment.find({ pg: user.pg, billDate: { '>=': startDate, '<=': endDate } })
                    .exec(function(err, payments) {
                        if (err) return cb(false, 0);

                        totalExpenses = _.reduce(payments, function(sum, payment) {
                            return parseFloat(payment.amount) + sum;
                        }, 0);
                        cb(false, Math.round(totalExpenses * 100));
                    });
            }
        };

        async.auto(queries, function(err, results) {
            if (err)
                return res.send({
                    error: true,
                    statusCode: 500,
                    statusMessage: 'Something went wrong'
                });

            var bedStatus = results.bedStatus;
            var income = results.income;
            var expenses = results.expenses;
            var profit = income - expenses;

            res.send({
                success: true,
                statusCode: 200,
                statusMessage: 'done successfully',
                data: {
                    bedStatus: bedStatus,
                    financeStatus: {
                        income: income,
                        expenses: expenses,
                        profit: profit,
                        startDate: startDate,
                        endDate: endDate
                    }
                }
            });
        });
    }
};
