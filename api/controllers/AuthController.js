/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var jwt = require('jsonwebtoken');
var hasher = require("password-hash");

module.exports = {
    login: function(req, res) {
        var email = req.body.email;
        var password = req.body.password;

        User.findOne({ email: email }).exec(function(err, user) {
            if (err)
                return res.send({
                    error: true,
                    statusCode: 500,
                    statusMessage: 'Something went wrong',
                    err: err
                });

            // compare passwords
            if (hasher.verify(password, user.password)) {
                res.send({
                    success: true,
                    statusCode: 200,
                    statusMessage: 'Logged in successfully',
                    data: {
                        user: user,
                        token: createToken(user)
                    }
                });
            } else {
                res.send({
                    error: true,
                    statusCode: 403,
                    statusMessage: 'Wrong password'
                });
            }
        });
    }
};

var createToken = function(user) {
    var secret = 'abcd132akjfadxZ';
    var algorithm = 'HS256';
    return jwt.sign({
            id: user.id,
            date: new Date().getTime().toString()
        },
        secret, {
            algorithm: algorithm
        }
    );
}
