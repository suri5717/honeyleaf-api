/**
 * PaymentController
 *
 * @description :: Server-side logic for managing payments
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var moment = require('moment');

module.exports = {
    index: function(req, res) {
        var user = req.user;
        if (!user)
            return res.send({
                error: true,
                statusCode: 404,
                statusMessage: 'Session Expired. Please login again!'
            });

        if (!user.isOwner())
            return res.send({
                error: true,
                statusCode: 403,
                statusMessage: 'Not authorized to view this data'
            });

        PgPayment.find({ where: { pg: user.pg }, sort: { billDate: 0 } })
            .exec(function(err, payments) {
                if (err)
                    return res.send({
                        error: true,
                        statusCode: 500,
                        statusMessage: "Something went wrong"
                    });

                res.send({
                    success: true,
                    statusCode: 200,
                    statusMessage: 'done successfully',
                    data: { payments: payments }
                });
            });
    },
    create: function(req, res) {
        var user = req.user;
        if (!user)
            return res.send({
                error: true,
                statusCode: 404,
                statusMessage: 'Session Expired. Please login again!'
            });

        if (!user.isOwner())
            return res.send({
                error: true,
                statusCode: 403,
                statusMessage: 'Not authorized to create pg payment'
            });

        var data = req.body;
        if (!data.amount)
            return res.send({
                error: true,
                statusCode: 400,
                statusMessage: 'Amount is missing'
            });

        var paymentData = {
            pg: user.pg,
            to: data.paidTo,
            amount: data.amount,
            category: data.category || 'others',
            billDate: data.billDate || moment().format('YYYY-MM-DD'),
            notes: data.notes || ''
        };

        PgPayment.create(paymentData)
            .exec(function(err, payment) {
                if (err || !payment)
                    return res.send({
                        error: true,
                        statusCode: 500,
                        statusMessage: 'Something went wrong'
                    });

                res.send({
                    success: true,
                    statusCode: 200,
                    statusMessage: 'Payment is added successfully',
                    data: { payment: payment.toJSON() }
                });
            });
    }
};
