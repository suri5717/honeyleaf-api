var passport = require('passport');
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;

var JWT_STRATEGY_CONFIG = {
    secretOrKey: 'abcd132akjfadxZ',
    jwtFromRequest: ExtractJwt.fromAuthHeader()
};

function _onJWTStrategy(payload, next) {
    User.findOne({ id: payload.id }).exec(function(err, user) {
        if (err || !user) {
            return next(null, null, { message: 'Incorrect token!' });
        }

        return next(null, user, { message: '' });
    });
}

passport.use(new JwtStrategy(JWT_STRATEGY_CONFIG, _onJWTStrategy));
