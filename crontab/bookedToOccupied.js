var _ = require('underscore');
var moment = require('moment');

module.exports = {
    run: function() {
        // Get all beds which are in booked state
        console.log('Running the cron');
        Bed.find({isBooked: true}).populate('bookedTenantId')
            .exec(function(err, beds) {
                _.each(beds, function(bed) {
                    var tenant = bed.bookedTenantId;
                    var startDate = tenant.fromDate;
                    var todayDate = moment().format('YYYY-MM-DD');
                    if (moment(startDate).isBefore(todayDate) || moment(startDate).isSame(todayDate)) {
                        // Change the tenant state from 'booked' to 'occupied'
                        tenant.status = 'occupied';
                        tenant.save();

                        //  Change the tenant properties at bed
                        bed.bookedTenantId = null;
                        bed.tenantId = tenant.id;
                        bed.isVacant = false;
                        bed.isBooked = false;
                        bed.save();
                    }
                });
            })
    }
};
