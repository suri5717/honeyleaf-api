# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.24)
# Database: honeyleaf
# Generation Time: 2016-03-10 14:23:29 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table beds
# ------------------------------------------------------------

DROP TABLE IF EXISTS `beds`;

CREATE TABLE `beds` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `room_id` bigint(20) NOT NULL,
  `tenant_id` bigint(20) DEFAULT NULL,
  `booked_tenant_id` bigint(20) DEFAULT NULL,
  `is_vacant` tinyint(1) NOT NULL DEFAULT '1',
  `is_booked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `beds_fk1` (`tenant_id`),
  CONSTRAINT `beds_fk1` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

LOCK TABLES `beds` WRITE;
/*!40000 ALTER TABLE `beds` DISABLE KEYS */;

INSERT INTO `beds` (`id`, `room_id`, `tenant_id`, `booked_tenant_id`, `is_vacant`, `is_booked`, `created_at`, `updated_at`)
VALUES
	(1,1,1,NULL,0,0,'2016-03-07 15:43:30','0000-00-00 00:00:00'),
	(2,1,NULL,NULL,1,0,'2016-03-07 16:02:07','0000-00-00 00:00:00'),
	(3,2,NULL,NULL,1,0,'2016-03-06 20:50:19','0000-00-00 00:00:00'),
	(4,2,NULL,NULL,1,0,'2016-03-06 20:50:19','0000-00-00 00:00:00'),
	(5,2,NULL,NULL,1,0,'2016-03-06 20:50:19','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `beds` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table paying_guests
# ------------------------------------------------------------

DROP TABLE IF EXISTS `paying_guests`;

CREATE TABLE `paying_guests` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `area` varchar(100) DEFAULT NULL,
  `city` varchar(100) NOT NULL,
  `address` varchar(256) NOT NULL,
  `mobile` varchar(10) NOT NULL DEFAULT '',
  `alt_mobile` varchar(10) DEFAULT NULL,
  `lat` decimal(9,6) NOT NULL,
  `lng` decimal(9,6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

LOCK TABLES `paying_guests` WRITE;
/*!40000 ALTER TABLE `paying_guests` DISABLE KEYS */;

INSERT INTO `paying_guests` (`id`, `name`, `area`, `city`, `address`, `mobile`, `alt_mobile`, `lat`, `lng`, `created_at`, `updated_at`)
VALUES
	(1,'Test PG','HSR','Bangalore','dummy address','8892150510',NULL,0.000000,0.000000,'2016-03-06 14:04:22','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `paying_guests` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pg_payments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pg_payments`;

CREATE TABLE `pg_payments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pg_id` bigint(20) NOT NULL,
  `to` varchar(200) NOT NULL,
  `category` varchar(50) NOT NULL,
  `amount` float(8,2) NOT NULL,
  `date` date NOT NULL,
  `notes` varchar(250) DEFAULT NULL,
  `bill_url` varchar(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `pg_payments_fk0` (`pg_id`),
  CONSTRAINT `pg_payments_fk0` FOREIGN KEY (`pg_id`) REFERENCES `paying_guests` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table rooms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `rooms`;

CREATE TABLE `rooms` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pg_id` bigint(20) NOT NULL,
  `name` varchar(20) NOT NULL DEFAULT '',
  `floor_no` int(2) NOT NULL,
  `cur_bed_count` int(2) NOT NULL,
  `max_bed_count` int(2) NOT NULL,
  `amenities` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `rooms_fk0` (`pg_id`),
  CONSTRAINT `rooms_fk0` FOREIGN KEY (`pg_id`) REFERENCES `paying_guests` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;

INSERT INTO `rooms` (`id`, `pg_id`, `name`, `floor_no`, `cur_bed_count`, `max_bed_count`, `amenities`, `created_at`, `updated_at`)
VALUES
	(1,1,'C2A',1,2,4,NULL,'2016-03-06 20:45:53','0000-00-00 00:00:00'),
	(2,1,'C2B',1,3,4,NULL,'2016-03-06 20:46:49','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tenant_payments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tenant_payments`;

CREATE TABLE `tenant_payments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tenant_id` bigint(20) DEFAULT NULL,
  `amount` int(6) NOT NULL,
  `month_paid_for` date NOT NULL,
  `paid_date` date NOT NULL,
  `type` varchar(50) NOT NULL DEFAULT 'monthly_rent',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `tenant_payments_fk0` (`tenant_id`),
  CONSTRAINT `tenant_payments_fk0` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

LOCK TABLES `tenant_payments` WRITE;
/*!40000 ALTER TABLE `tenant_payments` DISABLE KEYS */;

INSERT INTO `tenant_payments` (`id`, `tenant_id`, `amount`, `month_paid_for`, `paid_date`, `type`, `created_at`, `updated_at`)
VALUES
	(2,2,5500,'2016-03-01','2016-03-10','monthly_rent','2016-03-10 00:03:07','2016-03-10 00:03:07'),
	(3,2,3000,'2016-03-01','2016-03-10','deposit','2016-03-10 00:09:46','2016-03-10 00:09:46');

/*!40000 ALTER TABLE `tenant_payments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tenant_stay_history
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tenant_stay_history`;

CREATE TABLE `tenant_stay_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `tenant_id` bigint(20) NOT NULL,
  `bed_id` bigint(20) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `tenant_stay_history_fk0` (`tenant_id`),
  KEY `tenant_stay_history_fk1` (`bed_id`),
  CONSTRAINT `tenant_stay_history_fk0` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`id`),
  CONSTRAINT `tenant_stay_history_fk1` FOREIGN KEY (`bed_id`) REFERENCES `beds` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table tenants
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tenants`;

CREATE TABLE `tenants` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `alt_mobile` varchar(10) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'regular',
  `bed_id` int(10) DEFAULT NULL,
  `pg_id` int(10) DEFAULT '0',
  `photo_url` varchar(250) DEFAULT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `status` enum('occupied','on_notice','booked','left') DEFAULT 'occupied',
  `notice_start_date` date DEFAULT NULL,
  `notice_end_date` date DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

LOCK TABLES `tenants` WRITE;
/*!40000 ALTER TABLE `tenants` DISABLE KEYS */;

INSERT INTO `tenants` (`id`, `name`, `email`, `mobile`, `alt_mobile`, `address`, `type`, `bed_id`, `pg_id`, `photo_url`, `from_date`, `to_date`, `status`, `notice_start_date`, `notice_end_date`, `created_at`, `updated_at`)
VALUES
	(1,'Suri','suri5717@gmail.com','8892150510',NULL,'HSR','regular',1,1,NULL,'2016-03-07','0000-00-00','occupied',NULL,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(2,'Raj','raj@gmail.com','8899889988',NULL,'BTM','regular',4,1,NULL,'2016-03-06','0000-00-00','occupied',NULL,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `tenants` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL DEFAULT '',
  `mobile` varchar(10) NOT NULL DEFAULT '',
  `password` varchar(64) DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT 'owner',
  `otp_verified` tinyint(1) DEFAULT '0',
  `otp` varchar(64) DEFAULT NULL,
  `otp_expires` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pg_id` bigint(20) NOT NULL,
  `created_by` bigint(20) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pg_id` (`pg_id`),
  UNIQUE KEY `mobile` (`mobile`),
  UNIQUE KEY `email` (`email`),
  KEY `users_fk1` (`created_by`),
  CONSTRAINT `users_fk0` FOREIGN KEY (`pg_id`) REFERENCES `paying_guests` (`id`),
  CONSTRAINT `users_fk1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `password`, `type`, `otp_verified`, `otp`, `otp_expires`, `pg_id`, `created_by`, `created_at`, `updated_at`)
VALUES
	(1,'Suresh','suri5717@gmail.com','8892150510','NULL','owner',0,NULL,'2016-03-07 15:06:56',1,NULL,'0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
