ALTER TABLE `beds` ADD COLUMN `pg_id` bigint(20) NOT NULL AFTER `id`;
ALTER TABLE `beds` ADD CONSTRAINT `beds_fk2` FOREIGN KEY (`pg_id`) REFERENCES `paying_guests`(`id`);