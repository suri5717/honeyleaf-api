#!/usr/bin/env node

/**
 * Module dependencies
 */

var Sails = require('sails').constructor;
var fs = require('fs');
var parse = require('csv-parse');
var _ = require('underscore');

// global variables
var pgObj;
var userObj;
var roomsData = [];
var bedsData = [];
var tenantsData = [];
var roomTenants = {};
var roomNames = [];
var tenantDetails = [];

// Load app to get access to ORM, services, etc (but don't lift an actual server onto a port)
var app = new Sails();
app.load({
    hooks: { grunt: false },
    log: { level: 'warn' }
}, function sailsReady(err) {
    if (err) {
        console.error('Error loading app:', err);
        return process.exit(1);
    }

    var pgPayload = {
        name: 'Test PG',
        area: 'HSR Layout',
        city: 'Bangalore',
        address: 'Door No 126, 13th Main, 7th Cross',
        mobile: '8892150510',
        altMobile: '9940255210',
        type: 'gents',
        lat: '12.916591',
        lng: '77.637156'
    };

    PG.create(pgPayload).exec(function(err, pg) {
        if (err) {
            return console.log(err);
        } else {
            console.log(pg);
            pgObj = pg;
        }

        var userPayload = {
            name: 'Suresh Mopuru',
            email: 'suri5717@gmail.com',
            mobile: '8892150510',
            password: 'sure8892',
            type: 'owner',
            pg: pg.id
        };
        User.create(userPayload).exec(function(err, user) {
            if (err) {
                console.log(err);
            } else {
                console.log(user);
                userObj = user;
                setupRooms();
            }
        })
    });
});

var setupRooms = function() {
    var firstRow = true;
    fs.createReadStream('./tasks/database/rooms.csv')
        .pipe(parse({
            delimiter: ', ',
            auto_parse: true
        }))
        .on('data', function(csvrow) {
            if (!firstRow) {
                roomsData.push(csvrow);
            } else {
                firstRow = false;
            }
        }).on('end', function() {
            createRoom(roomsData.shift());
        });
};

var createRoom = function(data) {
    var data = {
        pg: pgObj.id,
        name: data.shift(),
        floorNo: data.shift(),
        curBedCount: data.shift(),
        maxBedCount: data.shift(),
        amenities: null
    };
    Room.create(data).exec(function(err, room) {
        // create max bed count no of beds
        for (var i = 0; i < room.curBedCount; i++) {
            bedsData.push({ roomId: room.id, pgId: pgObj.id });
        }

        // create More rooms
        if (roomsData.length) {
            createRoom(roomsData.shift());
        } else {
            createBed(bedsData.shift());
        }
    });
};

var createBed = function(data) {
    Bed.create(data).exec(function(err, bed) {
        if (bedsData.length) {
            createBed(bedsData.shift());
        } else {
            console.log('\nRooms and beds created successfully...\n');
            setupTenants();
        }
    });
};

var setupTenants = function() {
    var firstRow = true;
    fs.createReadStream('./tasks/database/tenants.csv')
        .pipe(parse({
            delimiter: '###,',
            auto_parse: true
        }))
        .on('data', function(csvrow) {
            if (!firstRow) {
                tenantsData.push(csvrow);
            } else {
                firstRow = false;
            }
        }).on('end', function() {
            groupTenantsByRoom();
        });
};

var groupTenantsByRoom = function() {
    _.each(tenantsData, function(tenantRow) {
        var roomName = tenantRow.shift();
        if (!roomTenants[roomName]) {
            roomTenants[roomName] = [];
        }
        var data = {
            name: tenantRow.shift(),
            email: tenantRow.shift() || null,
            mobile: tenantRow.shift() || null,
            altMobile: tenantRow.shift() || null,
            address: tenantRow.shift() || null,
            type: tenantRow.shift().toLowerCase(),
            pg: pgObj.id
        };
        data.altMobile = _.isString(data.altMobile) ? data.altMobile.replace(/[^0-9]/g, '') : data.altMobile;
        data.mobile = _.isString(data.mobile) ? data.mobile.replace(/[^0-9]/g, '') : data.mobile;
        if (!_.isNull(data.name)) data.name = data.name.charAt(0).toUpperCase() + data.name.slice(1).toLowerCase();
        if (!_.isNull(data.email)) data.email = data.email.toLowerCase();
        if (!_.isNull(data.address)) data.address = data.address.toLowerCase();
        var date = tenantRow.shift().split('-');
        data.fromDate = date[2] + '-' + date[1] + '-' + date[0];

        roomTenants[roomName].push(data);
    });
    roomNames = Object.keys(roomTenants);
    findRoom();
};

var findRoom = function() {
    var roomName = roomNames.shift();
    Room.findOne({ name: roomName }).populate('beds', { isVacant: true }).exec(function(err, room) {
        var i = 0;
        console.log(roomName);
        _.each(roomTenants[roomName], function(roomTenant) {
            roomTenant.bed = room.beds[i++].id;
            tenantDetails.push(_.clone(roomTenant));
        });
        if (roomNames.length) {
            findRoom();
        } else {
            createTenant();
        }
    });
};

var createTenant = function() {
    if (!tenantDetails.length) {
        console.log('\nTenants created and beds updated successfully...\n');
        console.log('\nTenant stay history also added successfully...\n');
        return;
    }

    var data = tenantDetails.shift();
    console.log(data);

    // create tenant
    Tenant.create(data).exec(function(err, tenant) {
        if (err)
            return console.log(err);

        // update bed
        var bedData = { tenantId: tenant.id, isVacant: false };
        Bed.update({ id: data.bed }, bedData).exec(function(err, bed) {});

        // add entry in tenant stay
        var stayData = {
            tenant: tenant.id,
            bed: data.bed,
            fromDate: data.fromDate
        };
        TenantStay.create(stayData).exec(function(err, tenantStay) {
            createTenant();
        });
    });
};
