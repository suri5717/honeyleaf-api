#!/usr/bin/env node

/**
 * Module dependencies
 */

var Sails = require('sails').constructor;
var fs = require('fs');
var parse = require('csv-parse');
var _ = require('underscore');

// global variables
var roomsData = [];
var bedsData = [];

// Load app to get access to ORM, services, etc (but don't lift an actual server onto a port)
var app = new Sails();
app.load({
    hooks: { grunt: false },
    log: { level: 'warn' }
}, function sailsReady(err) {
    if (err) {
        console.error('Error loading app:', err);
        return process.exit(1);
    }

    var firstRow = true;
    fs.createReadStream('./tasks/database/rooms.csv')
        .pipe(parse({
            delimiter: ', ',
            auto_parse: true
        }))
        .on('data', function(csvrow) {
            if (!firstRow) {
                roomsData.push(csvrow);
            } else {
                firstRow = false;
            }
        }).on('end', function() {
            createRoom(roomsData.shift());
        });
});

var createRoom = function(data) {
    var data = {
        pg: 1,
        name: data.shift(),
        floorNo: data.shift(),
        curBedCount: data.shift(),
        maxBedCount: data.shift(),
        amenities: null
    };
    Room.create(data).exec(function(err, room) {
        // create max bed count no of beds
        for (var i = 0; i < room.curBedCount; i++) {
            bedsData.push({ roomId: room.id });
        }

        // create More rooms
        if (roomsData.length) {
            createRoom(roomsData.shift());
        } else {
            createBed(bedsData.shift());
        }
    });
};

var createBed = function(data) {
    Bed.create(data).exec(function(err, bed) {
        if(bedsData.length){
            createBed(bedsData.shift());
        }else{
            console.log('\nRooms and beds created successfully...\n');
        }
    });
}
