#!/usr/bin/env node

/**
 * Module dependencies
 */

var Sails = require('sails').constructor;
var fs = require('fs');
var parse = require('csv-parse');
var _ = require('underscore');
// var moment = require('moment');

// global variables
var tenantsData = [];
var roomTenants = {};
var roomNames = [];
var tenantDetails = [];

// Load app to get access to ORM, services, etc (but don't lift an actual server onto a port)
var app = new Sails();
app.load({
    hooks: { grunt: false },
    log: { level: 'warn' }
}, function sailsReady(err) {
    if (err) {
        console.error('Error loading app:', err);
        return process.exit(1);
    }

    var firstRow = true;
    fs.createReadStream('./tasks/database/tenants.csv')
        .pipe(parse({
            delimiter: '###,',
            auto_parse: true
        }))
        .on('data', function(csvrow) {
            if (!firstRow) {
                tenantsData.push(csvrow);
            } else {
                firstRow = false;
            }
        }).on('end', function() {
            groupTenantsByRoom();
        });
});

var groupTenantsByRoom = function() {
    _.each(tenantsData, function(tenantRow) {
        var roomName = tenantRow.shift();
        if (!roomTenants[roomName]) {
            roomTenants[roomName] = [];
        }
        var data = {
            name: tenantRow.shift(),
            email: tenantRow.shift() || null,
            mobile: tenantRow.shift() || null,
            altMobile: tenantRow.shift() || null,
            address: tenantRow.shift() || null,
            type: tenantRow.shift().toLowerCase(),
            pg: 1
        };
        if(!_.isNull(data.name)) data.name = data.name.charAt(0).toUpperCase() + data.name.slice(1).toLowerCase();
        if(!_.isNull(data.email)) data.email = data.email.toLowerCase();
        if(!_.isNull(data.address)) data.address = data.address.toLowerCase();
        var date = tenantRow.shift().split('-');
        data.fromDate = date[2] + '-' + date[1] + '-' + date[0];

        roomTenants[roomName].push(data);
    });
    roomNames = Object.keys(roomTenants);
    findRoom();
};

var findRoom = function() {
    var roomName = roomNames.shift();
    Room.findOne({ name: roomName }).populate('beds', { isVacant: true }).exec(function(err, room) {
        var i = 0;
        console.log(roomName);
        _.each(roomTenants[roomName], function(roomTenant) {
            roomTenant.bed = room.beds[i++].id;
            tenantDetails.push(_.clone(roomTenant));
        });
        if (roomNames.length) {
            findRoom();
        } else {
            createTenant();
        }
    });
}

var createTenant = function() {
    if (!tenantDetails.length) {
        console.log('\nTenants created and beds updated successfully...\n');
        console.log('\nTenant stay history also added successfully...\n');
        return;
    }

    var data = tenantDetails.shift();
    console.log(data);

    // create tenant
    Tenant.create(data).exec(function(err, tenant) {
        if(err)
            return console.log(err);

        // update bed
        var bedData = { tenantId: tenant.id, isVacant: false };
        Bed.update({ id: data.bed }, bedData).exec(function(err, bed) {});

        // add entry in tenant stay
        var stayData = {
            tenant: tenant.id,
            bed: data.bed,
            fromDate: data.fromDate
        };
        TenantStay.create(stayData).exec(function(err, tenantStay) {
            createTenant();
        });
    });
};
